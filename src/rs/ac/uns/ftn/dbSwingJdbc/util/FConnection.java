package rs.ac.uns.ftn.dbSwingJdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FConnection {
	private static Connection instance=null;
	protected FConnection(){}
	
	public static Connection getInstance(){
		if(instance==null){
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				instance=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","oracle","oracle");
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
}
