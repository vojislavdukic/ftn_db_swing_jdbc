package rs.ac.uns.ftn.dbSwingJdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnikNaProjektu;
import rs.ac.uns.ftn.dbSwingJdbc.util.FConnection;

public class FRadnikNaProjektuDAO {
	public static List<FRadnikNaProjektu> getListRadnikNaProjektuByRadnik(Integer radnikId){
		List<FRadnikNaProjektu> result=new ArrayList<>();
		FRadnikNaProjektu radnikNaProjektu=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select spr,mbr,brc from radproj where mbr=?");
			ps.setInt(1, radnikId);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				radnikNaProjektu=new FRadnikNaProjektu();
				radnikNaProjektu.setProjekat(FProjekatDAO.getProjekatBasic(rs.getInt(1)));
				radnikNaProjektu.setRadnik(FRadnikDAO.getRadnikBasic(rs.getInt(2)));
				radnikNaProjektu.setBrojRadnihSati(rs.getInt(3));
				result.add(radnikNaProjektu);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static List<FRadnikNaProjektu> getListRadnikNaProjektuByProjekat(Integer projekatId){
		List<FRadnikNaProjektu> result=new ArrayList<>();
		FRadnikNaProjektu radnikNaProjektu=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select spr,mbr,brc from radproj where spr=?");
			ps.setInt(1, projekatId);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				radnikNaProjektu=new FRadnikNaProjektu();
				radnikNaProjektu.setProjekat(FProjekatDAO.getProjekatBasic(rs.getInt(1)));
				radnikNaProjektu.setRadnik(FRadnikDAO.getRadnikBasic(rs.getInt(2)));
				radnikNaProjektu.setBrojRadnihSati(rs.getInt(3));
				result.add(radnikNaProjektu);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static List<FRadnikNaProjektu> getListRadnikNaProjektu(){
		List<FRadnikNaProjektu> result=new ArrayList<>();
		FRadnikNaProjektu radnikNaProjektu=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select spr,mbr,brc from radproj");
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				radnikNaProjektu=new FRadnikNaProjektu();
				radnikNaProjektu.setProjekat(FProjekatDAO.getProjekatBasic(rs.getInt(1)));
				radnikNaProjektu.setRadnik(FRadnikDAO.getRadnikBasic(rs.getInt(2)));
				radnikNaProjektu.setBrojRadnihSati(rs.getInt(3));
				result.add(radnikNaProjektu);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static void insert(FRadnikNaProjektu radnikNaProjektu) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("insert into radproj (spr,mbr,brc) values (?,?,?)");
		if(radnikNaProjektu.getProjekat()!=null) ps.setInt(1, radnikNaProjektu.getProjekat().getSifraProjekta()); else ps.setNull(1, java.sql.Types.INTEGER);
		if(radnikNaProjektu.getRadnik()!=null) ps.setInt(2, radnikNaProjektu.getRadnik().getMbr()); else ps.setNull(2, java.sql.Types.INTEGER);
		if(radnikNaProjektu.getBrojRadnihSati()!=null) ps.setInt(3, radnikNaProjektu.getBrojRadnihSati()); else ps.setNull(3, java.sql.Types.INTEGER);
		ps.executeUpdate();
		ps.close();
	}
	
	public static void update(FRadnikNaProjektu radnikNaProjektu) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("update radproj set brc=? where spr=? and mbr=?");
		if(radnikNaProjektu.getBrojRadnihSati()!=null) ps.setInt(1, radnikNaProjektu.getBrojRadnihSati()); else ps.setNull(1, java.sql.Types.INTEGER);
		if(radnikNaProjektu.getProjekat()!=null) ps.setInt(2, radnikNaProjektu.getProjekat().getSifraProjekta()); else ps.setNull(2, java.sql.Types.INTEGER);
		if(radnikNaProjektu.getRadnik()!=null) ps.setInt(3, radnikNaProjektu.getRadnik().getMbr()); else ps.setNull(3, java.sql.Types.INTEGER);
		ps.executeUpdate();
		ps.close();
	}
	
	public static void delete(FRadnikNaProjektu radnikNaProjektu) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("delete from radproj where spr=? and mbr=?");
		ps.setInt(1, radnikNaProjektu.getProjekat().getSifraProjekta());
		ps.setInt(2, radnikNaProjektu.getRadnik().getMbr());
		ps.executeUpdate();
		ps.close();
	}
	
	public static void deleteByRadnik(FRadnikNaProjektu radnikNaProjektu) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("delete from radproj where  mbr=?");
		ps.setInt(1, radnikNaProjektu.getRadnik().getMbr());
		ps.executeUpdate();
		ps.close();
	}
	
	public static void deleteByProjekat(FRadnikNaProjektu radnikNaProjektu) throws SQLException{
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("delete from radproj where spr=?");
			ps.setInt(1, radnikNaProjektu.getProjekat().getSifraProjekta());
			ps.executeUpdate();
			ps.close();
	}
	
	public static String[] columns(){
		String[] result={"Radnik","Projekat","Broj radnih casova"};
		return result;
	}
	
	public static String[][] toTableData(List<FRadnikNaProjektu> radprojekti){
		String[][] result=new String[radprojekti.size()][3];
		for(int i=0;i<radprojekti.size();i++){
			result[i][0]=radprojekti.get(i).getRadnik().toString();
			result[i][1]=radprojekti.get(i).getProjekat().toString();
			result[i][2]=radprojekti.get(i).getBrojRadnihSati().toString();
		}
		return result;
	}
}
