package rs.ac.uns.ftn.dbSwingJdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.dbSwingJdbc.model.FProjekat;
import rs.ac.uns.ftn.dbSwingJdbc.util.FConnection;

public class FProjekatDAO {
	public static FProjekat getProjekatBasic(Integer id){
		FProjekat projekat=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select spr,ruk,nap,nar from projekat where spr=?");
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				projekat=new FProjekat();
				projekat.setSifraProjekta(rs.getInt(1));
				projekat.setNazivProjekta(rs.getString(3));
				projekat.setNarucilacProjekta(rs.getString(4));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return projekat;
	}
	
	public static FProjekat getProjekatRukovodilac(Integer id){
		FProjekat projekat=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select spr,ruk,nap,nar from projekat where spr=?");
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				projekat=new FProjekat();
				projekat.setSifraProjekta(rs.getInt(1));
				projekat.setRukovodilac(FRadnikDAO.getRadnikBasic(rs.getInt(2)));
				projekat.setNazivProjekta(rs.getString(3));
				projekat.setNarucilacProjekta(rs.getString(4));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return projekat;
	}
	
	public static List<FProjekat> getListProjekatRukovodilac(){
		List<FProjekat> result=new ArrayList<>();
		FProjekat projekat=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select spr,ruk,nap,nar from projekat");
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				projekat=new FProjekat();
				projekat.setSifraProjekta(rs.getInt(1));
				projekat.setRukovodilac(FRadnikDAO.getRadnikBasic(rs.getInt(2)));
				projekat.setNazivProjekta(rs.getString(3));
				projekat.setNarucilacProjekta(rs.getString(4));
				result.add(projekat);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static void insert(FProjekat projekat) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("insert into projekat (spr,ruk,nap,nar) values (?,?,?,?)");
		if(projekat.getSifraProjekta()!=null) ps.setInt(1, projekat.getSifraProjekta()); else ps.setNull(1, java.sql.Types.INTEGER);
		if(projekat.getRukovodilac()!=null) ps.setInt(2, projekat.getRukovodilac().getMbr()); else ps.setNull(2, java.sql.Types.INTEGER);
		if(projekat.getNazivProjekta()!=null) ps.setString(3, projekat.getNazivProjekta()); else ps.setNull(3, java.sql.Types.VARCHAR);
		if(projekat.getNarucilacProjekta()!=null) ps.setString(4, projekat.getNarucilacProjekta()); else ps.setNull(4, java.sql.Types.VARCHAR);
		ps.executeUpdate();
		ps.close();
	}
	
	public static void update(FProjekat projekat) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("update projekat set ruk=?,nap=?,nar=? where spr=?");
		if(projekat.getRukovodilac()!=null) ps.setInt(1, projekat.getRukovodilac().getMbr()); else ps.setNull(1, java.sql.Types.INTEGER);
		if(projekat.getNazivProjekta()!=null) ps.setString(2, projekat.getNazivProjekta()); else ps.setNull(2, java.sql.Types.VARCHAR);
		if(projekat.getNarucilacProjekta()!=null) ps.setString(3, projekat.getNarucilacProjekta()); else ps.setNull(3, java.sql.Types.VARCHAR);
		if(projekat.getSifraProjekta()!=null) ps.setInt(4, projekat.getSifraProjekta()); else ps.setNull(4, java.sql.Types.INTEGER);
		ps.executeUpdate();
		ps.close();
	}
	
	public static void delete(FProjekat projekat) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("delete from projekat where spr=?");
		ps.setInt(1, projekat.getSifraProjekta());
		ps.executeUpdate();
		ps.close();
	}
	
	public static String[] columns(){
		String[] result={"Sifra","Naziv","Narucilac"};
		return result;
	}
	
	public static String[][] toTableData(List<FProjekat> projekti){
		String[][] result=new String[projekti.size()][3];
		for(int i=0;i<projekti.size();i++){
			result[i][0]=projekti.get(i).getSifraProjekta().toString();
			result[i][1]=projekti.get(i).getNazivProjekta().toString();
			result[i][2]=projekti.get(i).getNarucilacProjekta().toString();
		}
		return result;
	}
}
