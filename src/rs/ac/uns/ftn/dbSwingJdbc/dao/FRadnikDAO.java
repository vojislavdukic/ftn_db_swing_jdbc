package rs.ac.uns.ftn.dbSwingJdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;
import rs.ac.uns.ftn.dbSwingJdbc.util.FConnection;

public class FRadnikDAO {
	
	public static FRadnik getRadnikBasic(Integer id){
		FRadnik radnik=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select mbr,ime,prz,sef,plt,pre,god from radnik where mbr=?");
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				radnik=new FRadnik();
				radnik.setMbr(rs.getInt(1));
				radnik.setIme(rs.getString(2));
				radnik.setPrezime(rs.getString(3));
				radnik.setPlata(rs.getInt(5));
				radnik.setPremija(rs.getInt(6));
				radnik.setGodinaRodjenja(rs.getDate(7));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return radnik;
	}
	
	public static FRadnik getRadnikSef(Integer id){
		FRadnik radnik=null;
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select mbr,ime,prz,sef,plt,pre,god from radnik where mbr=?");
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				radnik=new FRadnik();
				radnik.setMbr(rs.getInt(1));
				radnik.setIme(rs.getString(2));
				radnik.setPrezime(rs.getString(3));
				radnik.setSef(getRadnikBasic(rs.getInt(4)));
				radnik.setPlata(rs.getInt(5));
				radnik.setPremija(rs.getInt(6));
				radnik.setGodinaRodjenja(rs.getDate(7));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return radnik;
	}
	
	public static List<FRadnik> getListRadnikSef(){
		FRadnik radnik=null;
		List<FRadnik> result=new ArrayList<>();
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select mbr,ime,prz,sef,plt,pre,god from radnik");
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				radnik=new FRadnik();
				radnik.setMbr(rs.getInt(1));
				radnik.setIme(rs.getString(2));
				radnik.setPrezime(rs.getString(3));
				radnik.setSef(getRadnikBasic(rs.getInt(4)));
				radnik.setPlata(rs.getInt(5));
				radnik.setPremija(rs.getInt(6));
				radnik.setGodinaRodjenja(rs.getDate(7));
				result.add(radnik);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static List<FRadnik> getListRadnikSefSearch(String text){
		FRadnik radnik=null;
		List<FRadnik> result=new ArrayList<>();
		text = text.toLowerCase();
		try {
			PreparedStatement ps=FConnection.getInstance()
					.prepareStatement("select mbr,ime,prz,sef,plt,pre,god from radnik where "
							+ "lower(to_char(mbr)) like '%'||?||'%' or "
							+ "lower(to_char(ime)) like '%'||?||'%' or "
							+ "lower(to_char(prz)) like '%'||?||'%' or "
							+ "lower(to_char(sef)) like '%'||?||'%' or "
							+ "lower(to_char(plt)) like '%'||?||'%' or "
							+ "lower(to_char(pre)) like '%'||?||'%' or "
							+ "lower(to_char(god)) like '%'||?||'%'");
			ps.setString(1, text);
			ps.setString(2, text);
			ps.setString(3, text);
			ps.setString(4, text);
			ps.setString(5, text);
			ps.setString(6, text);
			ps.setString(7, text);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				radnik=new FRadnik();
				radnik.setMbr(rs.getInt(1));
				radnik.setIme(rs.getString(2));
				radnik.setPrezime(rs.getString(3));
				radnik.setSef(getRadnikBasic(rs.getInt(4)));
				radnik.setPlata(rs.getInt(5));
				radnik.setPremija(rs.getInt(6));
				radnik.setGodinaRodjenja(rs.getDate(7));
				result.add(radnik);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static void insert(FRadnik radnik) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("insert into radnik (mbr,ime,prz,sef,plt,pre,god) values (?,?,?,?,?,?,?)");
		if(radnik.getMbr()!=null) ps.setInt(1, radnik.getMbr()); else ps.setNull(1, java.sql.Types.INTEGER);
		if(radnik.getIme()!=null) ps.setString(2, radnik.getIme()); else ps.setNull(2, java.sql.Types.VARCHAR);
		if(radnik.getPrezime()!=null) ps.setString(3, radnik.getPrezime()); else ps.setNull(3, java.sql.Types.VARCHAR);
		if(radnik.getSef()!=null) ps.setInt(4, radnik.getSef().getMbr()); else ps.setNull(4, java.sql.Types.INTEGER);
		if(radnik.getPlata()!=null) ps.setInt(5, radnik.getPlata()); else ps.setNull(5, java.sql.Types.INTEGER);
		if(radnik.getPremija()!=null) ps.setInt(6, radnik.getPremija()); else ps.setNull(6, java.sql.Types.INTEGER);
		if(radnik.getGodinaRodjenja()!=null) ps.setDate(7, radnik.getGodinaRodjenja()); else ps.setNull(7, java.sql.Types.DATE);
		ps.executeUpdate();
		ps.close();
	}
	
	public static void update(FRadnik radnik) throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("update radnik set ime=?,prz=?,sef=?,plt=?,pre=?,god=? where mbr=?");
		if(radnik.getIme()!=null) ps.setString(1, radnik.getIme()); else ps.setNull(1, java.sql.Types.VARCHAR);
		if(radnik.getPrezime()!=null) ps.setString(2, radnik.getPrezime()); else ps.setNull(2, java.sql.Types.VARCHAR);
		if(radnik.getSef()!=null) ps.setInt(3, radnik.getSef().getMbr()); else ps.setNull(3, java.sql.Types.INTEGER);
		if(radnik.getPlata()!=null) ps.setInt(4, radnik.getPlata()); else ps.setNull(4, java.sql.Types.INTEGER);
		if(radnik.getPremija()!=null) ps.setInt(5, radnik.getPremija()); else ps.setNull(5, java.sql.Types.INTEGER);
		if(radnik.getGodinaRodjenja()!=null) ps.setDate(6, radnik.getGodinaRodjenja()); else ps.setNull(6, java.sql.Types.DATE);
		if(radnik.getMbr()!=null) ps.setInt(7, radnik.getMbr()); else ps.setNull(7, java.sql.Types.INTEGER);
		ps.executeUpdate();
		ps.close();
	}
	
	public static void delete(FRadnik radnik)  throws SQLException{
		PreparedStatement ps=FConnection.getInstance()
				.prepareStatement("delete from radnik where mbr=?");
		ps.setInt(1, radnik.getMbr());
		ps.executeUpdate();
		ps.close();
	}
	
	public static String[] columns(){
		String[] result={"Mbr","Ime","Prezime","Plata"};
		return result;
	}
	
	public static String[][] toTableData(List<FRadnik> radnici){
		String[][] result=new String[radnici.size()][4];
		for(int i=0;i<radnici.size();i++){
			result[i][0]=radnici.get(i).getMbr().toString();
			result[i][1]=radnici.get(i).getIme().toString();
			result[i][2]=radnici.get(i).getPrezime().toString();
			result[i][3]=radnici.get(i).getPlata().toString();
		}
		return result;
	}
}
