package rs.ac.uns.ftn.dbSwingJdbc.model;

public class FRadnikNaProjektu {
	private Integer brojRadnihSati;
	
	private FRadnik radnik;
	private FProjekat projekat;
	
	public Integer getBrojRadnihSati() {
		return brojRadnihSati;
	}
	public void setBrojRadnihSati(Integer brojRadnihSati) {
		this.brojRadnihSati = brojRadnihSati;
	}
	public FRadnik getRadnik() {
		return radnik;
	}
	public void setRadnik(FRadnik radnik) {
		this.radnik = radnik;
	}
	public FProjekat getProjekat() {
		return projekat;
	}
	public void setProjekat(FProjekat projekat) {
		this.projekat = projekat;
	}
	
	@Override
	public String toString() {
		return "FRadnikNaProjektu [brojRadnihSati=" + brojRadnihSati
				+ ", radnik=" + radnik + ", projekat=" + projekat + "]";
	}
	
}
