package rs.ac.uns.ftn.dbSwingJdbc.model;

import java.sql.Date;
import java.util.List;

public class FRadnik {
	private Integer mbr;
	private String ime;
	private String prezime;
	private Date godinaRodjenja;
	private Integer plata;
	private Integer premija;
	
	private FRadnik sef;
	private List<FProjekat> radiNa;
	private List<FProjekat> upravljaSa;
	
	public Integer getMbr() {
		return mbr;
	}
	public void setMbr(Integer mbr) {
		this.mbr = mbr;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Date getGodinaRodjenja() {
		return godinaRodjenja;
	}
	public void setGodinaRodjenja(Date godinaRodjenja) {
		this.godinaRodjenja = godinaRodjenja;
	}
	public Integer getPlata() {
		return plata;
	}
	public void setPlata(Integer plata) {
		this.plata = plata;
	}
	public Integer getPremija() {
		return premija;
	}
	public void setPremija(Integer premija) {
		this.premija = premija;
	}
	public FRadnik getSef() {
		return sef;
	}
	public void setSef(FRadnik sef) {
		this.sef = sef;
	}
	public List<FProjekat> getRadiNa() {
		return radiNa;
	}
	public void setRadiNa(List<FProjekat> radiNa) {
		this.radiNa = radiNa;
	}
	public List<FProjekat> getUpravljaSa() {
		return upravljaSa;
	}
	public void setUpravljaSa(List<FProjekat> upravljaSa) {
		this.upravljaSa = upravljaSa;
	}
	
	@Override
	public String toString() {
		return  (mbr!=null?mbr+". ":"")+ ime + " "+prezime;
	}
	
	
}
