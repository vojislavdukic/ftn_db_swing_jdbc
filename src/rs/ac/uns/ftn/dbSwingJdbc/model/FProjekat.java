package rs.ac.uns.ftn.dbSwingJdbc.model;

public class FProjekat {
	private Integer sifraProjekta;
	private String nazivProjekta;
	private String narucilacProjekta;
	
	private FRadnik rukovodilac;

	public Integer getSifraProjekta() {
		return sifraProjekta;
	}

	public void setSifraProjekta(Integer sifraProjekta) {
		this.sifraProjekta = sifraProjekta;
	}

	public String getNazivProjekta() {
		return nazivProjekta;
	}

	public void setNazivProjekta(String nazivProjekta) {
		this.nazivProjekta = nazivProjekta;
	}

	public String getNarucilacProjekta() {
		return narucilacProjekta;
	}

	public void setNarucilacProjekta(String narucilacProjekta) {
		this.narucilacProjekta = narucilacProjekta;
	}

	public FRadnik getRukovodilac() {
		return rukovodilac;
	}

	public void setRukovodilac(FRadnik rukovodilac) {
		this.rukovodilac = rukovodilac;
	}

	@Override
	public String toString() {
		return  (sifraProjekta!=null?sifraProjekta+". ":"")+ nazivProjekta;
	}
	
}
