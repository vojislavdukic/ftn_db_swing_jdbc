package rs.ac.uns.ftn.dbSwingJdbc.main;

import java.awt.EventQueue;

import javax.swing.UIManager;

import rs.ac.uns.ftn.dbSwingJdbc.gui.FMainForm;

public class FMain {

	public static void main(String[] args){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
			        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					FMainForm window = new FMainForm();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
