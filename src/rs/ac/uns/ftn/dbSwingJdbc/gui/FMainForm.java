package rs.ac.uns.ftn.dbSwingJdbc.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SpringLayout;

import rs.ac.uns.ftn.dbSwingJdbc.gui.projekat.FProjekatGlobalForm;
import rs.ac.uns.ftn.dbSwingJdbc.gui.radnik.FRadnikGlobalForm;
import rs.ac.uns.ftn.dbSwingJdbc.gui.radproj.FRadprojGlobalForm;
import javax.swing.JToolBar;
import javax.swing.JLabel;

public class FMainForm {

	private JFrame frmBazePodataka;
	private JMenu mnRadproj;
	private JMenuItem mntmGlobalniPrikaz_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FMainForm window = new FMainForm();
					window.frmBazePodataka.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FMainForm() {
		initialize();
		/*
		 * Run the app centralized
		 * */
		frmBazePodataka.setLocationRelativeTo(null);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBazePodataka = new JFrame();
		frmBazePodataka.setTitle("Baze podataka");
		frmBazePodataka.setResizable(false);
		frmBazePodataka.setVisible(true);
		frmBazePodataka.setBounds(100, 100, 315, 218);
		frmBazePodataka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frmBazePodataka.getContentPane().setLayout(springLayout);
		
		JToolBar statusBar = new JToolBar();
		springLayout.putConstraint(SpringLayout.WEST, statusBar, 0, SpringLayout.WEST, frmBazePodataka.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, statusBar, 0, SpringLayout.SOUTH, frmBazePodataka.getContentPane());
		frmBazePodataka.getContentPane().add(statusBar);
		
		JLabel lblBazePodataka = new JLabel("Baze podataka - Radnik / Projekat");
		statusBar.add(lblBazePodataka);
		
		JMenuBar menuBar = new JMenuBar();
		frmBazePodataka.setJMenuBar(menuBar);
		
		JMenu mnRadnici = new JMenu("Radnici");
		menuBar.add(mnRadnici);
		
		JMenuItem mntmGlobalniPrikaz = new JMenuItem("Globalni prikaz");
		mntmGlobalniPrikaz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FRadnikGlobalForm rgf=new FRadnikGlobalForm();
				rgf.setVisible(true);
			}
		});
		mnRadnici.add(mntmGlobalniPrikaz);
		
		JMenu mnProjekti = new JMenu("Projekti");
		menuBar.add(mnProjekti);
		
		JMenuItem mntmGlobalniPrikaz_1 = new JMenuItem("Globalni prikaz");
		mntmGlobalniPrikaz_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FProjekatGlobalForm pgf=new FProjekatGlobalForm();
				pgf.setVisible(true);
			}
		});
		mnProjekti.add(mntmGlobalniPrikaz_1);
		
		mnRadproj = new JMenu("Radproj");
		menuBar.add(mnRadproj);
		
		mntmGlobalniPrikaz_2 = new JMenuItem("Globalni prikaz");
		mntmGlobalniPrikaz_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FRadprojGlobalForm rpgf=new FRadprojGlobalForm();
				rpgf.setVisible(true);
			}
		});
		mnRadproj.add(mntmGlobalniPrikaz_2);
	}
}
