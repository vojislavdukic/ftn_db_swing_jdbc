package rs.ac.uns.ftn.dbSwingJdbc.gui.radproj;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import rs.ac.uns.ftn.dbSwingJdbc.dao.FProjekatDAO;
import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikDAO;
import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikNaProjektuDAO;
import rs.ac.uns.ftn.dbSwingJdbc.model.FProjekat;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnikNaProjektu;

public class FRadprojUpdateForm extends JDialog {

	private List<FRadnik> radnici;
	private List<FProjekat> projekti;
	private FRadnikNaProjektu radproj=null;
	private boolean isUpdate;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfBrc;
	private JComboBox cbRadnik;
	private JComboBox cbProjekat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FRadprojUpdateForm dialog = new FRadprojUpdateForm(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FRadprojUpdateForm(FRadnikNaProjektu radproj) {
		setTitle("Azuriranje radproj");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				load();
			}
		});
		this.radproj=radproj;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 316, 189);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblBrojRadnihasova = new JLabel("Broj radnih casova:");
			GridBagConstraints gbc_lblBrojRadnihasova = new GridBagConstraints();
			gbc_lblBrojRadnihasova.anchor = GridBagConstraints.EAST;
			gbc_lblBrojRadnihasova.insets = new Insets(0, 0, 5, 5);
			gbc_lblBrojRadnihasova.gridx = 0;
			gbc_lblBrojRadnihasova.gridy = 0;
			contentPanel.add(lblBrojRadnihasova, gbc_lblBrojRadnihasova);
		}
		{
			tfBrc = new JTextField();
			GridBagConstraints gbc_tfBrc = new GridBagConstraints();
			gbc_tfBrc.insets = new Insets(0, 0, 5, 0);
			gbc_tfBrc.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfBrc.gridx = 1;
			gbc_tfBrc.gridy = 0;
			contentPanel.add(tfBrc, gbc_tfBrc);
			tfBrc.setColumns(10);
		}
		{
			JLabel lblRadnik = new JLabel("Radnik:");
			GridBagConstraints gbc_lblRadnik = new GridBagConstraints();
			gbc_lblRadnik.anchor = GridBagConstraints.EAST;
			gbc_lblRadnik.insets = new Insets(0, 0, 5, 5);
			gbc_lblRadnik.gridx = 0;
			gbc_lblRadnik.gridy = 1;
			contentPanel.add(lblRadnik, gbc_lblRadnik);
		}
		{
			cbRadnik = new JComboBox();
			GridBagConstraints gbc_cbRadnik = new GridBagConstraints();
			gbc_cbRadnik.insets = new Insets(0, 0, 5, 0);
			gbc_cbRadnik.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbRadnik.gridx = 1;
			gbc_cbRadnik.gridy = 1;
			contentPanel.add(cbRadnik, gbc_cbRadnik);
		}
		{
			JLabel lblProjekat = new JLabel("Projekat:");
			GridBagConstraints gbc_lblProjekat = new GridBagConstraints();
			gbc_lblProjekat.anchor = GridBagConstraints.EAST;
			gbc_lblProjekat.insets = new Insets(0, 0, 5, 5);
			gbc_lblProjekat.gridx = 0;
			gbc_lblProjekat.gridy = 2;
			contentPanel.add(lblProjekat, gbc_lblProjekat);
		}
		{
			cbProjekat = new JComboBox();
			GridBagConstraints gbc_cbProjekat = new GridBagConstraints();
			gbc_cbProjekat.insets = new Insets(0, 0, 5, 0);
			gbc_cbProjekat.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbProjekat.gridx = 1;
			gbc_cbProjekat.gridy = 2;
			contentPanel.add(cbProjekat, gbc_cbProjekat);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Potvrdi");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							save();
							//close form
							FRadprojUpdateForm.this.setVisible(false);
							FRadprojUpdateForm.this.dispatchEvent(new WindowEvent(FRadprojUpdateForm.this, WindowEvent.WINDOW_CLOSING));
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(FRadprojUpdateForm.this, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Zatvori");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadprojUpdateForm.this.setVisible(false);
						FRadprojUpdateForm.this.dispatchEvent(new WindowEvent(FRadprojUpdateForm.this, WindowEvent.WINDOW_CLOSING));
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		/*
		 * Run the app centralized
		 * */
		setLocationRelativeTo(null);
	}
	
	private void load(){
		
		isUpdate=radproj!=null;
		radnici=FRadnikDAO.getListRadnikSef();
		projekti=FProjekatDAO.getListProjekatRukovodilac();
		
		FRadnik[] rListRadnik=new FRadnik[radnici.size()];
		for(int i=0;i<radnici.size();i++) rListRadnik[i]=radnici.get(i);
		DefaultComboBoxModel<FRadnik> cbmRadnik=new DefaultComboBoxModel<FRadnik>(rListRadnik);
		cbRadnik.setModel(cbmRadnik);
		
		FProjekat[] rListProjekat=new FProjekat[projekti.size()];
		for(int i=0;i<projekti.size();i++) rListProjekat[i]=projekti.get(i);
		DefaultComboBoxModel<FProjekat> cbmProjekat=new DefaultComboBoxModel<FProjekat>(rListProjekat);
		cbProjekat.setModel(cbmProjekat);
		
		if(isUpdate){
			cbRadnik.setEnabled(false);
			cbProjekat.setEnabled(false);
			
			tfBrc.setText(radproj.getBrojRadnihSati()!=null?radproj.getBrojRadnihSati().toString():"");

			for(int i=1;i<rListRadnik.length;i++){
				if(rListRadnik[i].getMbr().equals(radproj.getRadnik().getMbr())){
					cbmRadnik.setSelectedItem(rListRadnik[i]);
					break;
				}
			}
			
			for(int i=1;i<rListProjekat.length;i++){
				if(rListProjekat[i].getSifraProjekta().equals(radproj.getProjekat().getSifraProjekta())){
					cbmProjekat.setSelectedItem(rListProjekat[i]);
					break;
				}
			}
		}else{
			radproj=new FRadnikNaProjektu();
		}	
		
	}
	
	private void save() throws Exception{
		radproj.setBrojRadnihSati(tfBrc.getText().equals("")?null:Integer.parseInt(tfBrc.getText()));

		radproj.setRadnik((FRadnik)cbRadnik.getSelectedItem());
		radproj.setProjekat((FProjekat)cbProjekat.getSelectedItem());
		
		if(isUpdate){
			FRadnikNaProjektuDAO.update(radproj);
		}else{
			FRadnikNaProjektuDAO.insert(radproj);
		}
	}
	
	public JComboBox getCbRadnik() {
		return cbRadnik;
	}
	public JComboBox getCbProjekat() {
		return cbProjekat;
	}
}
