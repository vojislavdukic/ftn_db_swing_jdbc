package rs.ac.uns.ftn.dbSwingJdbc.gui.radproj;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import rs.ac.uns.ftn.dbSwingJdbc.dao.FProjekatDAO;
import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikDAO;
import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikNaProjektuDAO;
import rs.ac.uns.ftn.dbSwingJdbc.model.FProjekat;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnikNaProjektu;

public class FRadprojGlobalForm extends JDialog {

	private List<FRadnikNaProjektu> radprojekti=null;
	private FRadnikNaProjektu radproj=null;
	
	private final JPanel contentPanel = new JPanel();
	private JPanel panel;
	private JPanel panel_1;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnUnesi;
	private JButton btnIzmeni;
	private JButton btnObrisi;
	private JSeparator separator;
	private JLabel lblRadnik;
	private JLabel lblProjekat;
	private JLabel lblBrc;
	private JLabel lradnik;
	private JLabel lprojekat;
	private JLabel lbrc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FRadprojGlobalForm dialog = new FRadprojGlobalForm();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FRadprojGlobalForm() {
		setTitle("Radproj - globalni prikaz");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				refresh();
			}
		});
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 729, 405);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		
		panel_1 = new JPanel();
		contentPanel.add(panel_1);
		SpringLayout sl_panel_1 = new SpringLayout();
		panel_1.setLayout(sl_panel_1);
		
		scrollPane = new JScrollPane();
		sl_panel_1.putConstraint(SpringLayout.NORTH, scrollPane, 8, SpringLayout.NORTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.WEST, scrollPane, 10, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, scrollPane, -10, SpringLayout.SOUTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.EAST, scrollPane, -10, SpringLayout.EAST, panel_1);
		panel_1.add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(0, 0));
		contentPanel.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblRadnik = new JLabel("Radnik:");
		GridBagConstraints gbc_lblRadnik = new GridBagConstraints();
		gbc_lblRadnik.anchor = GridBagConstraints.EAST;
		gbc_lblRadnik.insets = new Insets(0, 0, 5, 5);
		gbc_lblRadnik.gridx = 0;
		gbc_lblRadnik.gridy = 0;
		panel.add(lblRadnik, gbc_lblRadnik);
		
		lradnik = new JLabel("");
		GridBagConstraints gbc_lradnik = new GridBagConstraints();
		gbc_lradnik.anchor = GridBagConstraints.WEST;
		gbc_lradnik.insets = new Insets(0, 0, 5, 0);
		gbc_lradnik.gridx = 1;
		gbc_lradnik.gridy = 0;
		panel.add(lradnik, gbc_lradnik);
		
		lblProjekat = new JLabel("Projekat:");
		GridBagConstraints gbc_lblProjekat = new GridBagConstraints();
		gbc_lblProjekat.anchor = GridBagConstraints.EAST;
		gbc_lblProjekat.insets = new Insets(0, 0, 5, 5);
		gbc_lblProjekat.gridx = 0;
		gbc_lblProjekat.gridy = 1;
		panel.add(lblProjekat, gbc_lblProjekat);
		
		lprojekat = new JLabel("");
		GridBagConstraints gbc_lprojekat = new GridBagConstraints();
		gbc_lprojekat.anchor = GridBagConstraints.WEST;
		gbc_lprojekat.insets = new Insets(0, 0, 5, 0);
		gbc_lprojekat.gridx = 1;
		gbc_lprojekat.gridy = 1;
		panel.add(lprojekat, gbc_lprojekat);
		
		lblBrc = new JLabel("Broj radnih casova:");
		GridBagConstraints gbc_lblBrc = new GridBagConstraints();
		gbc_lblBrc.anchor = GridBagConstraints.EAST;
		gbc_lblBrc.insets = new Insets(0, 0, 5, 5);
		gbc_lblBrc.gridx = 0;
		gbc_lblBrc.gridy = 2;
		panel.add(lblBrc, gbc_lblBrc);
		
		lbrc = new JLabel("");
		GridBagConstraints gbc_lbrc = new GridBagConstraints();
		gbc_lbrc.anchor = GridBagConstraints.WEST;
		gbc_lbrc.insets = new Insets(0, 0, 5, 0);
		gbc_lbrc.gridx = 1;
		gbc_lbrc.gridy = 2;
		panel.add(lbrc, gbc_lbrc);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Zatvori");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadprojGlobalForm.this.setVisible(false);
						FRadprojGlobalForm.this.dispatchEvent(new WindowEvent(FRadprojGlobalForm.this, WindowEvent.WINDOW_CLOSING));
					}
				});
				
				btnUnesi = new JButton("Unesi");
				btnUnesi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadprojUpdateForm editForm=new FRadprojUpdateForm(null);
						editForm.setVisible(true);
						refresh();
					}
				});
				buttonPane.add(btnUnesi);
				
				btnIzmeni = new JButton("Izmeni");
				btnIzmeni.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadprojUpdateForm editForm=new FRadprojUpdateForm(radproj);
						editForm.setVisible(true);
						refresh();
					}
				});
				btnIzmeni.setEnabled(false);
				buttonPane.add(btnIzmeni);
				
				btnObrisi = new JButton("Obrisi");
				btnObrisi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int dialogResult = JOptionPane.showConfirmDialog (null, "Da li zelite da obrisete radnika sa projekta?","Warning",JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION){
							try {
								FRadnikNaProjektuDAO.delete(radproj);
							} catch (SQLException e1) {
								JOptionPane.showMessageDialog(FRadprojGlobalForm.this, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
							}
							refresh();
						}
					}
				});
				btnObrisi.setEnabled(false);
				buttonPane.add(btnObrisi);
				
				separator = new JSeparator();
				buttonPane.add(separator);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		/*
		 * Run the app centralized
		 * */
		setLocationRelativeTo(null);
	}
	public JTable getTable() {
		return table;
	}
	
	public List<FRadnikNaProjektu> getRadprojekti() {
		return radprojekti;
	}

	public void setRadprojekti(List<FRadnikNaProjektu> radprojekti) {
		this.radprojekti = radprojekti;
	}

	public FRadnikNaProjektu getRadproj() {
		return radproj;
	}

	public void setRadproj(FRadnikNaProjektu radproj) {
		this.radproj = radproj;
	}

	private void refresh(){
		radprojekti=null;
		radproj=null;
		getBtnObrisi().setEnabled(false);
		getBtnIzmeni().setEnabled(false);
		
		radprojekti=FRadnikNaProjektuDAO.getListRadnikNaProjektu();
		DefaultTableModel tm=new DefaultTableModel(FRadnikNaProjektuDAO.toTableData(radprojekti), FRadnikNaProjektuDAO.columns()){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(tm);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow()!=-1){
					radproj=radprojekti.get(table.getSelectedRow());
					refreshLabel();
					getBtnObrisi().setEnabled(true);
					getBtnIzmeni().setEnabled(true);
				}
			}
		});
	}
	
	private void refreshLabel(){
		lradnik.setText(radproj.getRadnik().toString());
		lprojekat.setText(radproj.getProjekat().toString());
		lbrc.setText(radproj.getBrojRadnihSati().toString());
	}
	public JButton getBtnObrisi() {
		return btnObrisi;
	}
	public JButton getBtnIzmeni() {
		return btnIzmeni;
	}
}
