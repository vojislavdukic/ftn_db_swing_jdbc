package rs.ac.uns.ftn.dbSwingJdbc.gui.projekat;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import rs.ac.uns.ftn.dbSwingJdbc.dao.FProjekatDAO;
import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikDAO;
import rs.ac.uns.ftn.dbSwingJdbc.model.FProjekat;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;

public class FProjekatUpdateForm extends JDialog {

	private List<FRadnik> radnici;
	private FProjekat projekat=null;
	private boolean isUpdate;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfSifra;
	private JTextField tfNaziv;
	private JTextField tfNarucilac;
	private JComboBox cbRukovodilac;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FProjekatUpdateForm dialog = new FProjekatUpdateForm(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FProjekatUpdateForm(FProjekat projekat) {
		setTitle("Azuriranje projekta");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				load();
			}
		});
		this.projekat=projekat;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 316, 258);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblifraProjekta = new JLabel("Sifra projekta:");
			GridBagConstraints gbc_lblifraProjekta = new GridBagConstraints();
			gbc_lblifraProjekta.anchor = GridBagConstraints.EAST;
			gbc_lblifraProjekta.insets = new Insets(0, 0, 5, 5);
			gbc_lblifraProjekta.gridx = 0;
			gbc_lblifraProjekta.gridy = 0;
			contentPanel.add(lblifraProjekta, gbc_lblifraProjekta);
		}
		{
			tfSifra = new JTextField();
			GridBagConstraints gbc_tfSifra = new GridBagConstraints();
			gbc_tfSifra.insets = new Insets(0, 0, 5, 0);
			gbc_tfSifra.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfSifra.gridx = 1;
			gbc_tfSifra.gridy = 0;
			contentPanel.add(tfSifra, gbc_tfSifra);
			tfSifra.setColumns(10);
		}
		{
			JLabel lblNaziv = new JLabel("Naziv:");
			GridBagConstraints gbc_lblNaziv = new GridBagConstraints();
			gbc_lblNaziv.anchor = GridBagConstraints.EAST;
			gbc_lblNaziv.insets = new Insets(0, 0, 5, 5);
			gbc_lblNaziv.gridx = 0;
			gbc_lblNaziv.gridy = 1;
			contentPanel.add(lblNaziv, gbc_lblNaziv);
		}
		{
			tfNaziv = new JTextField();
			GridBagConstraints gbc_tfNaziv = new GridBagConstraints();
			gbc_tfNaziv.insets = new Insets(0, 0, 5, 0);
			gbc_tfNaziv.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfNaziv.gridx = 1;
			gbc_tfNaziv.gridy = 1;
			contentPanel.add(tfNaziv, gbc_tfNaziv);
			tfNaziv.setColumns(10);
		}
		{
			JLabel lblNaruilac = new JLabel("Narucilac:");
			GridBagConstraints gbc_lblNaruilac = new GridBagConstraints();
			gbc_lblNaruilac.anchor = GridBagConstraints.EAST;
			gbc_lblNaruilac.insets = new Insets(0, 0, 5, 5);
			gbc_lblNaruilac.gridx = 0;
			gbc_lblNaruilac.gridy = 2;
			contentPanel.add(lblNaruilac, gbc_lblNaruilac);
		}
		{
			tfNarucilac = new JTextField();
			GridBagConstraints gbc_tfNarucilac = new GridBagConstraints();
			gbc_tfNarucilac.insets = new Insets(0, 0, 5, 0);
			gbc_tfNarucilac.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfNarucilac.gridx = 1;
			gbc_tfNarucilac.gridy = 2;
			contentPanel.add(tfNarucilac, gbc_tfNarucilac);
			tfNarucilac.setColumns(10);
		}
		{
			JLabel lblRukovodilac = new JLabel("Rukovodilac:");
			GridBagConstraints gbc_lblRukovodilac = new GridBagConstraints();
			gbc_lblRukovodilac.insets = new Insets(0, 0, 0, 5);
			gbc_lblRukovodilac.anchor = GridBagConstraints.EAST;
			gbc_lblRukovodilac.gridx = 0;
			gbc_lblRukovodilac.gridy = 3;
			contentPanel.add(lblRukovodilac, gbc_lblRukovodilac);
		}
		{
			cbRukovodilac = new JComboBox();
			GridBagConstraints gbc_cbRukovodilac = new GridBagConstraints();
			gbc_cbRukovodilac.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbRukovodilac.gridx = 1;
			gbc_cbRukovodilac.gridy = 3;
			contentPanel.add(cbRukovodilac, gbc_cbRukovodilac);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Potvrdi");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							save();
							//close form
							FProjekatUpdateForm.this.setVisible(false);
							FProjekatUpdateForm.this.dispatchEvent(new WindowEvent(FProjekatUpdateForm.this, WindowEvent.WINDOW_CLOSING));
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(FProjekatUpdateForm.this, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
						}
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Zatvori");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FProjekatUpdateForm.this.setVisible(false);
						FProjekatUpdateForm.this.dispatchEvent(new WindowEvent(FProjekatUpdateForm.this, WindowEvent.WINDOW_CLOSING));
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		/*
		 * Run the app centralized
		 * */
		setLocationRelativeTo(null);
	}
	
	private void load(){
		
		isUpdate=projekat!=null;
		radnici=FRadnikDAO.getListRadnikSef();
		
		FRadnik bezRukovodioca=new FRadnik();
		bezRukovodioca.setIme("Bez rukovodioca");
		bezRukovodioca.setPrezime("");
		radnici.add(0, bezRukovodioca);
		FRadnik[] rList=new FRadnik[radnici.size()];
		for(int i=0;i<radnici.size();i++) rList[i]=radnici.get(i);
		DefaultComboBoxModel<FRadnik> cbm=new DefaultComboBoxModel<FRadnik>(rList);
		cbRukovodilac.setModel(cbm);
		
		if(isUpdate){
			tfSifra.setEnabled(false);
			
			tfSifra.setText(projekat.getSifraProjekta()!=null?projekat.getSifraProjekta().toString():"");
			tfNaziv.setText(projekat.getNazivProjekta()!=null?projekat.getNazivProjekta().toString():"");
			tfNarucilac.setText(projekat.getNarucilacProjekta()!=null?projekat.getNarucilacProjekta().toString():"");
			
			if(projekat.getRukovodilac()!=null){
				for(int i=1;i<rList.length;i++){
					if(rList[i].getMbr().equals(projekat.getRukovodilac().getMbr())){
						cbm.setSelectedItem(rList[i]);
						break;
					}
				}
			}
		}else{
			projekat=new FProjekat();
		}	
		
	}
	
	private void save() throws Exception{
		projekat.setSifraProjekta(tfSifra.getText().equals("")?null:Integer.parseInt(tfSifra.getText()));
		projekat.setNazivProjekta(tfNaziv.getText().equals("")?null:tfNaziv.getText());
		projekat.setNarucilacProjekta(tfNarucilac.getText().equals("")?null:tfNarucilac.getText());
		
		FRadnik selected=(FRadnik)cbRukovodilac.getSelectedItem();
		projekat.setRukovodilac(selected.getMbr()!=null?selected:null);
		
		if(isUpdate){
			FProjekatDAO.update(projekat);
		}else{
			FProjekatDAO.insert(projekat);
		}
	}
	
	public JComboBox getCbRukovodilac() {
		return cbRukovodilac;
	}
}
