package rs.ac.uns.ftn.dbSwingJdbc.gui.projekat;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import rs.ac.uns.ftn.dbSwingJdbc.dao.FProjekatDAO;
import rs.ac.uns.ftn.dbSwingJdbc.model.FProjekat;

public class FProjekatGlobalForm extends JDialog {

	private List<FProjekat> projekti=null;
	private FProjekat projekat=null;
	
	private final JPanel contentPanel = new JPanel();
	private JPanel panel;
	private JPanel panel_1;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnUnesi;
	private JButton btnIzmeni;
	private JButton btnObrisi;
	private JSeparator separator;
	private JLabel lblifraProjekta;
	private JLabel lblNaziv;
	private JLabel lblNarucilac;
	private JLabel lblRukovodilac;
	private JLabel lsifra;
	private JLabel lnaziv;
	private JLabel lnarucilac;
	private JLabel lrukovodilac;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FProjekatGlobalForm dialog = new FProjekatGlobalForm();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FProjekatGlobalForm() {
		setTitle("Projekti - globalni prikaz");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				refresh();
			}
		});
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 603, 405);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		
		panel_1 = new JPanel();
		contentPanel.add(panel_1);
		SpringLayout sl_panel_1 = new SpringLayout();
		panel_1.setLayout(sl_panel_1);
		
		scrollPane = new JScrollPane();
		sl_panel_1.putConstraint(SpringLayout.NORTH, scrollPane, 8, SpringLayout.NORTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.WEST, scrollPane, 10, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, scrollPane, -10, SpringLayout.SOUTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.EAST, scrollPane, -10, SpringLayout.EAST, panel_1);
		panel_1.add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(0, 0));
		contentPanel.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblifraProjekta = new JLabel("Sifra projekta:");
		GridBagConstraints gbc_lblifraProjekta = new GridBagConstraints();
		gbc_lblifraProjekta.anchor = GridBagConstraints.EAST;
		gbc_lblifraProjekta.insets = new Insets(0, 0, 5, 5);
		gbc_lblifraProjekta.gridx = 0;
		gbc_lblifraProjekta.gridy = 0;
		panel.add(lblifraProjekta, gbc_lblifraProjekta);
		
		lsifra = new JLabel("");
		GridBagConstraints gbc_lsifra = new GridBagConstraints();
		gbc_lsifra.anchor = GridBagConstraints.WEST;
		gbc_lsifra.insets = new Insets(0, 0, 5, 0);
		gbc_lsifra.gridx = 1;
		gbc_lsifra.gridy = 0;
		panel.add(lsifra, gbc_lsifra);
		
		lblNaziv = new JLabel("Naziv:");
		GridBagConstraints gbc_lblNaziv = new GridBagConstraints();
		gbc_lblNaziv.anchor = GridBagConstraints.EAST;
		gbc_lblNaziv.insets = new Insets(0, 0, 5, 5);
		gbc_lblNaziv.gridx = 0;
		gbc_lblNaziv.gridy = 1;
		panel.add(lblNaziv, gbc_lblNaziv);
		
		lnaziv = new JLabel("");
		GridBagConstraints gbc_lnaziv = new GridBagConstraints();
		gbc_lnaziv.anchor = GridBagConstraints.WEST;
		gbc_lnaziv.insets = new Insets(0, 0, 5, 0);
		gbc_lnaziv.gridx = 1;
		gbc_lnaziv.gridy = 1;
		panel.add(lnaziv, gbc_lnaziv);
		
		lblNarucilac = new JLabel("Narucilac:");
		GridBagConstraints gbc_lblNarucilac = new GridBagConstraints();
		gbc_lblNarucilac.anchor = GridBagConstraints.EAST;
		gbc_lblNarucilac.insets = new Insets(0, 0, 5, 5);
		gbc_lblNarucilac.gridx = 0;
		gbc_lblNarucilac.gridy = 2;
		panel.add(lblNarucilac, gbc_lblNarucilac);
		
		lnarucilac = new JLabel("");
		GridBagConstraints gbc_lnarucilac = new GridBagConstraints();
		gbc_lnarucilac.anchor = GridBagConstraints.WEST;
		gbc_lnarucilac.insets = new Insets(0, 0, 5, 0);
		gbc_lnarucilac.gridx = 1;
		gbc_lnarucilac.gridy = 2;
		panel.add(lnarucilac, gbc_lnarucilac);
		
		lblRukovodilac = new JLabel("Rukovodilac:");
		GridBagConstraints gbc_lblRukovodilac = new GridBagConstraints();
		gbc_lblRukovodilac.anchor = GridBagConstraints.EAST;
		gbc_lblRukovodilac.insets = new Insets(0, 0, 5, 5);
		gbc_lblRukovodilac.gridx = 0;
		gbc_lblRukovodilac.gridy = 3;
		panel.add(lblRukovodilac, gbc_lblRukovodilac);
		
		lrukovodilac = new JLabel("");
		GridBagConstraints gbc_lrukovodja = new GridBagConstraints();
		gbc_lrukovodja.anchor = GridBagConstraints.WEST;
		gbc_lrukovodja.insets = new Insets(0, 0, 5, 0);
		gbc_lrukovodja.gridx = 1;
		gbc_lrukovodja.gridy = 3;
		panel.add(lrukovodilac, gbc_lrukovodja);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Zatvori");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FProjekatGlobalForm.this.setVisible(false);
						FProjekatGlobalForm.this.dispatchEvent(new WindowEvent(FProjekatGlobalForm.this, WindowEvent.WINDOW_CLOSING));
					}
				});
				
				btnUnesi = new JButton("Unesi");
				btnUnesi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FProjekatUpdateForm editForm=new FProjekatUpdateForm(null);
						editForm.setVisible(true);
						refresh();
					}
				});
				buttonPane.add(btnUnesi);
				
				btnIzmeni = new JButton("Izmeni");
				btnIzmeni.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FProjekatUpdateForm editForm=new FProjekatUpdateForm(projekat);
						editForm.setVisible(true);
						refresh();
					}
				});
				btnIzmeni.setEnabled(false);
				buttonPane.add(btnIzmeni);
				
				btnObrisi = new JButton("Obris\u00A1");
				btnObrisi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int dialogResult = JOptionPane.showConfirmDialog (null, "Da li zelite da obrisete projekat?","Warning",JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION){
							try {
								FProjekatDAO.delete(projekat);
							} catch (SQLException e1) {
								JOptionPane.showMessageDialog(FProjekatGlobalForm.this, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
							}
							refresh();
						}
					}
				});
				btnObrisi.setEnabled(false);
				buttonPane.add(btnObrisi);
				
				separator = new JSeparator();
				buttonPane.add(separator);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		/*
		 * Run the app centralized
		 * */
		setLocationRelativeTo(null);
	}
	public JTable getTable() {
		return table;
	}
	
	public List<FProjekat> getRadnici() {
		return projekti;
	}

	public void setRadnici(List<FProjekat> projekti) {
		this.projekti = projekti;
	}

	public FProjekat getRadnik() {
		return projekat;
	}

	public void setRadnik(FProjekat projekat) {
		this.projekat = projekat;
	}
	
	private void refresh(){
		projekti=null;
		projekat=null;
		getBtnObrisi().setEnabled(false);
		getBtnIzmeni().setEnabled(false);
		
		projekti=FProjekatDAO.getListProjekatRukovodilac();
		DefaultTableModel tm=new DefaultTableModel(FProjekatDAO.toTableData(projekti), FProjekatDAO.columns()){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(tm);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow()!=-1){
					projekat=projekti.get(table.getSelectedRow());
					refreshLabel();
					getBtnObrisi().setEnabled(true);
					getBtnIzmeni().setEnabled(true);
				}
			}
		});
	}
	
	private void refreshLabel(){
		lsifra.setText(projekat.getSifraProjekta().toString());
		lnaziv.setText(projekat.getNazivProjekta().toString());
		lnarucilac.setText(projekat.getNarucilacProjekta().toString());
		lrukovodilac.setText(projekat.getRukovodilac()!=null?projekat.getRukovodilac().getIme()+" "+projekat.getRukovodilac().getPrezime():"");
	}
	public JButton getBtnObrisi() {
		return btnObrisi;
	}
	public JButton getBtnIzmeni() {
		return btnIzmeni;
	}
}
