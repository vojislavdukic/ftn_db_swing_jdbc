package rs.ac.uns.ftn.dbSwingJdbc.gui.radnik;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikDAO;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;

public class FRadnikGlobalForm extends JDialog {

	private List<FRadnik> radnici=null;
	private FRadnik radnik=null;
	
	private final JPanel contentPanel = new JPanel();
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblMatiniBroj;
	private JLabel lblIme;
	private JLabel lblPrezime;
	private JLabel lblef;
	private JLabel lblPlata;
	private JLabel lblPremija;
	private JLabel lblGodinaRoenja;
	private JScrollPane scrollPane;
	private JTable table;
	private JLabel lmbr;
	private JLabel lime;
	private JLabel lprezime;
	private JLabel lsef;
	private JLabel lplata;
	private JLabel lpremija;
	private JLabel lgodina;
	private JButton btnUnesi;
	private JButton btnIzmeni;
	private JButton btnObrisi;
	private JSeparator separator;
	private JTextField tfTrazi;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FRadnikGlobalForm dialog = new FRadnikGlobalForm();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FRadnikGlobalForm() {
		setTitle("Radnici - globalni prikaz");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				refresh(tfTrazi.getText());
			}
		});
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 603, 405);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);
		
		panel_1 = new JPanel();
		sl_contentPanel.putConstraint(SpringLayout.NORTH, panel_1, 5, SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, panel_1, 5, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, panel_1, 333, SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, panel_1, 297, SpringLayout.WEST, contentPanel);
		contentPanel.add(panel_1);
		SpringLayout sl_panel_1 = new SpringLayout();
		panel_1.setLayout(sl_panel_1);
		
		scrollPane = new JScrollPane();
		sl_panel_1.putConstraint(SpringLayout.NORTH, scrollPane, 39, SpringLayout.NORTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.WEST, scrollPane, 10, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, scrollPane, -10, SpringLayout.SOUTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.EAST, scrollPane, -10, SpringLayout.EAST, panel_1);
		panel_1.add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		tfTrazi = new JTextField();
		sl_panel_1.putConstraint(SpringLayout.WEST, tfTrazi, 10, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, tfTrazi, -9, SpringLayout.NORTH, scrollPane);
		panel_1.add(tfTrazi);
		tfTrazi.setColumns(10);
		
		JButton btnTrazi = new JButton("Trazi");
		btnTrazi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refresh(tfTrazi.getText());
			}
		});
		sl_panel_1.putConstraint(SpringLayout.SOUTH, btnTrazi, -6, SpringLayout.NORTH, scrollPane);
		sl_panel_1.putConstraint(SpringLayout.EAST, tfTrazi, -6, SpringLayout.WEST, btnTrazi);
		sl_panel_1.putConstraint(SpringLayout.EAST, btnTrazi, 0, SpringLayout.EAST, scrollPane);
		panel_1.add(btnTrazi);
		
		panel = new JPanel();
		sl_contentPanel.putConstraint(SpringLayout.NORTH, panel, 5, SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, panel, 297, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, panel, 333, SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, panel, 589, SpringLayout.WEST, contentPanel);
		panel.setPreferredSize(new Dimension(0, 0));
		contentPanel.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblMatiniBroj = new JLabel("Maticni broj:");
		GridBagConstraints gbc_lblMatiniBroj = new GridBagConstraints();
		gbc_lblMatiniBroj.anchor = GridBagConstraints.EAST;
		gbc_lblMatiniBroj.insets = new Insets(0, 0, 5, 5);
		gbc_lblMatiniBroj.gridx = 0;
		gbc_lblMatiniBroj.gridy = 0;
		panel.add(lblMatiniBroj, gbc_lblMatiniBroj);
		
		lmbr = new JLabel("");
		GridBagConstraints gbc_lmbr = new GridBagConstraints();
		gbc_lmbr.anchor = GridBagConstraints.WEST;
		gbc_lmbr.insets = new Insets(0, 0, 5, 0);
		gbc_lmbr.gridx = 1;
		gbc_lmbr.gridy = 0;
		panel.add(lmbr, gbc_lmbr);
		
		lblIme = new JLabel("Ime:");
		GridBagConstraints gbc_lblIme = new GridBagConstraints();
		gbc_lblIme.anchor = GridBagConstraints.EAST;
		gbc_lblIme.insets = new Insets(0, 0, 5, 5);
		gbc_lblIme.gridx = 0;
		gbc_lblIme.gridy = 1;
		panel.add(lblIme, gbc_lblIme);
		
		lime = new JLabel("");
		GridBagConstraints gbc_lime = new GridBagConstraints();
		gbc_lime.anchor = GridBagConstraints.WEST;
		gbc_lime.insets = new Insets(0, 0, 5, 0);
		gbc_lime.gridx = 1;
		gbc_lime.gridy = 1;
		panel.add(lime, gbc_lime);
		
		lblPrezime = new JLabel("Prezime:");
		GridBagConstraints gbc_lblPrezime = new GridBagConstraints();
		gbc_lblPrezime.anchor = GridBagConstraints.EAST;
		gbc_lblPrezime.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrezime.gridx = 0;
		gbc_lblPrezime.gridy = 2;
		panel.add(lblPrezime, gbc_lblPrezime);
		
		lprezime = new JLabel("");
		GridBagConstraints gbc_lprezime = new GridBagConstraints();
		gbc_lprezime.anchor = GridBagConstraints.WEST;
		gbc_lprezime.insets = new Insets(0, 0, 5, 0);
		gbc_lprezime.gridx = 1;
		gbc_lprezime.gridy = 2;
		panel.add(lprezime, gbc_lprezime);
		
		lblef = new JLabel("Sef:");
		GridBagConstraints gbc_lblef = new GridBagConstraints();
		gbc_lblef.anchor = GridBagConstraints.EAST;
		gbc_lblef.insets = new Insets(0, 0, 5, 5);
		gbc_lblef.gridx = 0;
		gbc_lblef.gridy = 3;
		panel.add(lblef, gbc_lblef);
		
		lsef = new JLabel("");
		GridBagConstraints gbc_lsef = new GridBagConstraints();
		gbc_lsef.anchor = GridBagConstraints.WEST;
		gbc_lsef.insets = new Insets(0, 0, 5, 0);
		gbc_lsef.gridx = 1;
		gbc_lsef.gridy = 3;
		panel.add(lsef, gbc_lsef);
		
		lblPlata = new JLabel("Plata:");
		GridBagConstraints gbc_lblPlata = new GridBagConstraints();
		gbc_lblPlata.anchor = GridBagConstraints.EAST;
		gbc_lblPlata.insets = new Insets(0, 0, 5, 5);
		gbc_lblPlata.gridx = 0;
		gbc_lblPlata.gridy = 4;
		panel.add(lblPlata, gbc_lblPlata);
		
		lplata = new JLabel("");
		GridBagConstraints gbc_lplata = new GridBagConstraints();
		gbc_lplata.anchor = GridBagConstraints.WEST;
		gbc_lplata.insets = new Insets(0, 0, 5, 0);
		gbc_lplata.gridx = 1;
		gbc_lplata.gridy = 4;
		panel.add(lplata, gbc_lplata);
		
		lblPremija = new JLabel("Premija:");
		GridBagConstraints gbc_lblPremija = new GridBagConstraints();
		gbc_lblPremija.anchor = GridBagConstraints.EAST;
		gbc_lblPremija.insets = new Insets(0, 0, 5, 5);
		gbc_lblPremija.gridx = 0;
		gbc_lblPremija.gridy = 5;
		panel.add(lblPremija, gbc_lblPremija);
		
		lpremija = new JLabel("");
		GridBagConstraints gbc_lpremija = new GridBagConstraints();
		gbc_lpremija.anchor = GridBagConstraints.WEST;
		gbc_lpremija.insets = new Insets(0, 0, 5, 0);
		gbc_lpremija.gridx = 1;
		gbc_lpremija.gridy = 5;
		panel.add(lpremija, gbc_lpremija);
		
		lblGodinaRoenja = new JLabel("Godina rodjenja:");
		GridBagConstraints gbc_lblGodinaRoenja = new GridBagConstraints();
		gbc_lblGodinaRoenja.insets = new Insets(0, 0, 0, 5);
		gbc_lblGodinaRoenja.anchor = GridBagConstraints.EAST;
		gbc_lblGodinaRoenja.gridx = 0;
		gbc_lblGodinaRoenja.gridy = 6;
		panel.add(lblGodinaRoenja, gbc_lblGodinaRoenja);
		
		lgodina = new JLabel("");
		GridBagConstraints gbc_lgodina = new GridBagConstraints();
		gbc_lgodina.anchor = GridBagConstraints.WEST;
		gbc_lgodina.gridx = 1;
		gbc_lgodina.gridy = 6;
		panel.add(lgodina, gbc_lgodina);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Zatvori");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadnikGlobalForm.this.setVisible(false);
						FRadnikGlobalForm.this.dispatchEvent(new WindowEvent(FRadnikGlobalForm.this, WindowEvent.WINDOW_CLOSING));
					}
				});
				
				btnUnesi = new JButton("Unesi");
				btnUnesi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadnikUpdateForm editForm=new FRadnikUpdateForm(null);
						editForm.setVisible(true);
						refresh(tfTrazi.getText());
					}
				});
				buttonPane.add(btnUnesi);
				
				btnIzmeni = new JButton("Izmeni");
				btnIzmeni.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadnikUpdateForm editForm=new FRadnikUpdateForm(radnik);
						editForm.setVisible(true);
						refresh(tfTrazi.getText());
					}
				});
				btnIzmeni.setEnabled(false);
				buttonPane.add(btnIzmeni);
				
				btnObrisi = new JButton("Obrisi");
				btnObrisi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int dialogResult = JOptionPane.showConfirmDialog (null, "Da li zelite da obrisete radnika?","Warning",JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION){
							try {
								FRadnikDAO.delete(radnik);
							} catch (SQLException e1) {
								JOptionPane.showMessageDialog(FRadnikGlobalForm.this, e1.getMessage(),"Delete error",JOptionPane.ERROR_MESSAGE);
							}
							refresh(tfTrazi.getText());
						}
					}
				});
				btnObrisi.setEnabled(false);
				buttonPane.add(btnObrisi);
				
				separator = new JSeparator();
				buttonPane.add(separator);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		/*
		 * Run the app centralized
		 * */
		setLocationRelativeTo(null);
	}
	public JTable getTable() {
		return table;
	}
	
	public List<FRadnik> getRadnici() {
		return radnici;
	}

	public void setRadnici(List<FRadnik> radnici) {
		this.radnici = radnici;
	}

	public FRadnik getRadnik() {
		return radnik;
	}

	public void setRadnik(FRadnik radnik) {
		this.radnik = radnik;
	}
	
	private void refresh(String search){
		radnici=null;
		radnik=null;
		getBtnObrisi().setEnabled(false);
		getBtnIzmeni().setEnabled(false);
		
		if(search==null || search.equals("")){
			radnici=FRadnikDAO.getListRadnikSef();
		}else{
			radnici=FRadnikDAO.getListRadnikSefSearch(search);
		}
		DefaultTableModel tm=new DefaultTableModel(FRadnikDAO.toTableData(radnici), FRadnikDAO.columns()){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(tm);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow()!=-1){
					radnik=radnici.get(table.getSelectedRow());
					refreshLabel();
					getBtnObrisi().setEnabled(true);
					getBtnIzmeni().setEnabled(true);
				}
			}
		});
	}
	
	private void refreshLabel(){
		lmbr.setText(radnik.getMbr().toString());
		lime.setText(radnik.getIme().toString());
		lprezime.setText(radnik.getPrezime().toString());
		lplata.setText(radnik.getPlata()!=null?radnik.getPlata().toString():"");
		lpremija.setText(radnik.getPremija()!=null?radnik.getPremija().toString():"");
		lsef.setText(radnik.getSef()!=null?radnik.getSef().getIme()+" "+radnik.getSef().getPrezime():"");
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		lgodina.setText(radnik.getGodinaRodjenja()!=null?sdf.format(radnik.getGodinaRodjenja()):"");
	}
	public JButton getBtnObrisi() {
		return btnObrisi;
	}
	public JButton getBtnIzmeni() {
		return btnIzmeni;
	}
	public JTextField getTfTrazi() {
		return tfTrazi;
	}
}
