package rs.ac.uns.ftn.dbSwingJdbc.gui.radnik;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import rs.ac.uns.ftn.dbSwingJdbc.dao.FRadnikDAO;
import rs.ac.uns.ftn.dbSwingJdbc.model.FRadnik;

public class FRadnikUpdateForm extends JDialog {

	private List<FRadnik> radnici;
	private FRadnik radnik=null;
	private boolean isUpdate;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfMbr;
	private JTextField tfIme;
	private JTextField tfPrezime;
	private JTextField tfPlata;
	private JTextField tfPremija;
	private JFormattedTextField tfDatumRodjenja;
	private JComboBox cbSef;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FRadnikUpdateForm dialog = new FRadnikUpdateForm(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FRadnikUpdateForm(FRadnik radnik) {
		setTitle("Azuriranje radnika");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				load();
			}
		});
		this.radnik=radnik;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 316, 258);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblMatiniBroj = new JLabel("Maticni broj:");
			GridBagConstraints gbc_lblMatiniBroj = new GridBagConstraints();
			gbc_lblMatiniBroj.anchor = GridBagConstraints.EAST;
			gbc_lblMatiniBroj.insets = new Insets(0, 0, 5, 5);
			gbc_lblMatiniBroj.gridx = 0;
			gbc_lblMatiniBroj.gridy = 0;
			contentPanel.add(lblMatiniBroj, gbc_lblMatiniBroj);
		}
		{
			tfMbr = new JTextField();
			GridBagConstraints gbc_tfMbr = new GridBagConstraints();
			gbc_tfMbr.insets = new Insets(0, 0, 5, 0);
			gbc_tfMbr.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfMbr.gridx = 1;
			gbc_tfMbr.gridy = 0;
			contentPanel.add(tfMbr, gbc_tfMbr);
			tfMbr.setColumns(10);
		}
		{
			JLabel lblIme = new JLabel("Ime:");
			GridBagConstraints gbc_lblIme = new GridBagConstraints();
			gbc_lblIme.anchor = GridBagConstraints.EAST;
			gbc_lblIme.insets = new Insets(0, 0, 5, 5);
			gbc_lblIme.gridx = 0;
			gbc_lblIme.gridy = 1;
			contentPanel.add(lblIme, gbc_lblIme);
		}
		{
			tfIme = new JTextField();
			GridBagConstraints gbc_tfIme = new GridBagConstraints();
			gbc_tfIme.insets = new Insets(0, 0, 5, 0);
			gbc_tfIme.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfIme.gridx = 1;
			gbc_tfIme.gridy = 1;
			contentPanel.add(tfIme, gbc_tfIme);
			tfIme.setColumns(10);
		}
		{
			JLabel lblPrezime = new JLabel("Prezime:");
			GridBagConstraints gbc_lblPrezime = new GridBagConstraints();
			gbc_lblPrezime.anchor = GridBagConstraints.EAST;
			gbc_lblPrezime.insets = new Insets(0, 0, 5, 5);
			gbc_lblPrezime.gridx = 0;
			gbc_lblPrezime.gridy = 2;
			contentPanel.add(lblPrezime, gbc_lblPrezime);
		}
		{
			tfPrezime = new JTextField();
			GridBagConstraints gbc_tfPrezime = new GridBagConstraints();
			gbc_tfPrezime.insets = new Insets(0, 0, 5, 0);
			gbc_tfPrezime.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfPrezime.gridx = 1;
			gbc_tfPrezime.gridy = 2;
			contentPanel.add(tfPrezime, gbc_tfPrezime);
			tfPrezime.setColumns(10);
		}
		{
			JLabel lblDatumRoenja = new JLabel("Datum rodjenja:");
			GridBagConstraints gbc_lblDatumRoenja = new GridBagConstraints();
			gbc_lblDatumRoenja.anchor = GridBagConstraints.EAST;
			gbc_lblDatumRoenja.insets = new Insets(0, 0, 5, 5);
			gbc_lblDatumRoenja.gridx = 0;
			gbc_lblDatumRoenja.gridy = 3;
			contentPanel.add(lblDatumRoenja, gbc_lblDatumRoenja);
		}
		{
			tfDatumRodjenja = new JFormattedTextField();
			GridBagConstraints gbc_tfDatumRodjenja = new GridBagConstraints();
			gbc_tfDatumRodjenja.insets = new Insets(0, 0, 5, 0);
			gbc_tfDatumRodjenja.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfDatumRodjenja.gridx = 1;
			gbc_tfDatumRodjenja.gridy = 3;
			contentPanel.add(tfDatumRodjenja, gbc_tfDatumRodjenja);
		}
		{
			JLabel lblPlata = new JLabel("Plata:");
			GridBagConstraints gbc_lblPlata = new GridBagConstraints();
			gbc_lblPlata.anchor = GridBagConstraints.EAST;
			gbc_lblPlata.insets = new Insets(0, 0, 5, 5);
			gbc_lblPlata.gridx = 0;
			gbc_lblPlata.gridy = 4;
			contentPanel.add(lblPlata, gbc_lblPlata);
		}
		{
			tfPlata = new JTextField();
			GridBagConstraints gbc_tfPlata = new GridBagConstraints();
			gbc_tfPlata.insets = new Insets(0, 0, 5, 0);
			gbc_tfPlata.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfPlata.gridx = 1;
			gbc_tfPlata.gridy = 4;
			contentPanel.add(tfPlata, gbc_tfPlata);
			tfPlata.setColumns(10);
		}
		{
			JLabel lblPremija = new JLabel("Premija:");
			GridBagConstraints gbc_lblPremija = new GridBagConstraints();
			gbc_lblPremija.anchor = GridBagConstraints.EAST;
			gbc_lblPremija.insets = new Insets(0, 0, 5, 5);
			gbc_lblPremija.gridx = 0;
			gbc_lblPremija.gridy = 5;
			contentPanel.add(lblPremija, gbc_lblPremija);
		}
		{
			tfPremija = new JTextField();
			GridBagConstraints gbc_tfPremija = new GridBagConstraints();
			gbc_tfPremija.insets = new Insets(0, 0, 5, 0);
			gbc_tfPremija.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfPremija.gridx = 1;
			gbc_tfPremija.gridy = 5;
			contentPanel.add(tfPremija, gbc_tfPremija);
			tfPremija.setColumns(10);
		}
		{
			JLabel lblef = new JLabel("Sef:");
			GridBagConstraints gbc_lblef = new GridBagConstraints();
			gbc_lblef.insets = new Insets(0, 0, 0, 5);
			gbc_lblef.anchor = GridBagConstraints.EAST;
			gbc_lblef.gridx = 0;
			gbc_lblef.gridy = 6;
			contentPanel.add(lblef, gbc_lblef);
		}
		{
			cbSef = new JComboBox();
			GridBagConstraints gbc_cbSef = new GridBagConstraints();
			gbc_cbSef.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbSef.gridx = 1;
			gbc_cbSef.gridy = 6;
			contentPanel.add(cbSef, gbc_cbSef);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Potvrdi");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							save();
							//close form
							FRadnikUpdateForm.this.setVisible(false);
							FRadnikUpdateForm.this.dispatchEvent(new WindowEvent(FRadnikUpdateForm.this, WindowEvent.WINDOW_CLOSING));
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(FRadnikUpdateForm.this, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
						}
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Zatvori");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FRadnikUpdateForm.this.setVisible(false);
						FRadnikUpdateForm.this.dispatchEvent(new WindowEvent(FRadnikUpdateForm.this, WindowEvent.WINDOW_CLOSING));
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		/*
		 * Run the app centralized
		 * */
		setLocationRelativeTo(null);
	}
	
	private void load(){
		isUpdate=radnik!=null;
		radnici=FRadnikDAO.getListRadnikSef();
		
		FRadnik bezSefa=new FRadnik();
		bezSefa.setIme("Bez sefa");
		bezSefa.setPrezime("");
		radnici.add(0, bezSefa);
		FRadnik[] rList=new FRadnik[radnici.size()];
		for(int i=0;i<radnici.size();i++) rList[i]=radnici.get(i);
		DefaultComboBoxModel<FRadnik> cbm=new DefaultComboBoxModel<FRadnik>(rList);
		cbSef.setModel(cbm);
		
		if(isUpdate){
			tfMbr.setEnabled(false);
			
			tfMbr.setText(radnik.getMbr()!=null?radnik.getMbr().toString():"");
			tfIme.setText(radnik.getIme()!=null?radnik.getIme().toString():"");
			tfPrezime.setText(radnik.getPrezime()!=null?radnik.getPrezime().toString():"");
			tfPlata.setText(radnik.getPlata()!=null?radnik.getPlata().toString():"");
			tfPremija.setText(radnik.getPremija()!=null?radnik.getPremija().toString():"");
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			tfDatumRodjenja.setText(radnik.getGodinaRodjenja()!=null?sdf.format(radnik.getGodinaRodjenja()):"");
			
			if(radnik.getSef()!=null){
				for(int i=1;i<rList.length;i++){
					if(rList[i].getMbr().equals(radnik.getSef().getMbr())){
						cbm.setSelectedItem(rList[i]);
						break;
					}
				}
			}
		}else{
			radnik=new FRadnik();
		}		
	}
	
	private void save() throws Exception{
		radnik.setMbr(tfMbr.getText().equals("")?null:Integer.parseInt(tfMbr.getText()));
		radnik.setIme(tfIme.getText().equals("")?null:tfIme.getText());
		radnik.setPrezime(tfPrezime.getText().equals("")?null:tfPrezime.getText());
		radnik.setPlata(tfPlata.getText().equals("")?null:Integer.parseInt(tfPlata.getText()));
		radnik.setPremija(tfPremija.getText().equals("")?null:Integer.parseInt(tfPremija.getText()));
		
		//potential parse exception
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		radnik.setGodinaRodjenja(tfDatumRodjenja.getText().equals("")?null:new java.sql.Date(sdf.parse(tfDatumRodjenja.getText()).getTime()));
		
		FRadnik selected=(FRadnik)cbSef.getSelectedItem();
		radnik.setSef(selected.getMbr()!=null?selected:null);
		
		if(isUpdate){
			FRadnikDAO.update(radnik);
		}else{
			FRadnikDAO.insert(radnik);
		}
	}
	

	public JFormattedTextField getTfDatumRodjenja() {
		return tfDatumRodjenja;
	}
	public JComboBox getCbSef() {
		return cbSef;
	}
}
